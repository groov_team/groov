package com.example.methodologyproject;


import android.view.View;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class DB_TEST {
    Home _home;

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void init() {
        _home = new Home();
        activityActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, _home)
                .commit();

    }

    @Test
    public void test_loadGenres(){
        assertTrue(_home.getGenres().contains("K-POP"));
    }

    @Test
    public void test_loadAlbums(){
        assertTrue(_home.getAlbums().contains("I just want to pop"));
    }

    @Test
    public void test_loadArtist(){
        assertTrue(_home.getArtist().contains("The Bops"));
    }

    @Test
    public void test_addSong(){
        _home.addASong("Dearly Beloved");
        assertTrue(_home.addASong("Dearly Beloved").contains("Dearly Beloved"));
    }



}