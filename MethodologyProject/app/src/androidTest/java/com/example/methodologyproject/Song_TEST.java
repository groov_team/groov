package com.example.methodologyproject;


import android.view.View;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class Song_TEST {
    Home _home;

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void init() {
        _home = new Home();
        activityActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, _home)
                .commit();
    }

    @Test
    public void test_SongClass(){
        Song newSong = new Song(1, "Bananas", "Kayla H.", "Food Basket", "Blues & Soul", "3.45", "Song_TEST", "idk", "Mountains fom Montana");
        int checker;

        newSong.setSongId(0);
        assertEquals(0, newSong.getSongId());

        newSong.setSongName("Apples");
        checker = newSong.getSongName().compareTo("Apples");
        assertEquals(0, checker);

        newSong.setArtist("Kay H.");
        checker = newSong.getArtist().compareTo("Kay H.");
        assertEquals(0, checker);

        newSong.setAlbum("Nom Noms");
        checker = newSong.getAlbum().compareTo("Nom Noms");
        assertEquals(0, checker);

        newSong.setGenre("Just Blues");
        checker = newSong.getGenre().compareTo("Just Blues");
        assertEquals(0, checker);

        newSong.setTime("4.15");
        checker = newSong.getTime().compareTo("4.15");
        assertEquals(0, checker);

        newSong.setStatus("still single");
        checker = newSong.getStatus().compareTo("still single");
        assertEquals(0, checker);

        newSong.setFile("soft meth");
        checker = newSong.getFile().compareTo("soft meth");
        assertEquals(0, checker);

        newSong.setAlbumArtwork("Farmer's Market");
        checker = newSong.getAlbumArtwork().compareTo("Farmer's Market");
        assertEquals(0, checker);


    }
}