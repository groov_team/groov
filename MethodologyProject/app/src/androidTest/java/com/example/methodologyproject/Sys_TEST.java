package com.example.methodologyproject;


import android.widget.Button;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
public class Sys_TEST {
    Home _home;
    DBHelper _db;
    ArrayList<String> LikedList = new ArrayList<>();


    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void init() {
        LikedList.add("K-POP");
        LikedList.add("THE BOPS");
        LikedList.add("I JUST WANT TO POP");
        _home = new Home();
        activityActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, _home)
                .commit();
    }

    @Test
    public void test_System(){
        String genre = "K-POP";
        String artist = "THE BOPS";
        String album = "I JUST WANT TO POP";
        onView(withId(R.id.button3)).perform(scrollTo());
        onView(withId(R.id.vert)).perform(scrollTo());
        onView(withId(R.id.center)).perform(scrollTo());
        onView(withId(R.id.button3)).perform(scrollTo());
        onView(withId(R.id.vert)).perform(scrollTo());
        onView(withId(R.id.button7)).perform(scrollTo());
        onView(withId(R.id.button3)).perform(scrollTo());
        onView(withId(R.id.vert)).perform(scrollTo());
        onView(withId(R.id.Groov)).perform(click());

        assertTrue(LikedList.contains(genre));
        assertTrue(LikedList.contains(artist));
        assertTrue(LikedList.contains(album));
    }
}