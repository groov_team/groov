package com.example.methodologyproject;

import android.view.View;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class HomeTest {

    Home _home;

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void init(){
        _home = new Home();
        activityActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction()
            .add(android.R.id.content, _home)
            .commit();

    }

    @Test
    public void test_onCreateView() {
        //test for swipe ***CAN ONLY DO 2 AT A TIME
        //onView(withId(R.id.hori)).perform(swipeLeft()).equals(0);
        //onView(withId(R.id.hori)).perform(swipeRight()).equals(1);
        //onView(withId(R.id.vert)).perform(swipeUp()).equals(2);
        //onView(withId(R.id.vert)).perform(swipeDown()).equals(3);
    }

    @Test
    public void test_Help() {
        int i = 0;
        assertEquals(i, 0);
    }

}