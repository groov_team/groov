package com.example.methodologyproject;

import java.sql.Blob;

public class Song {
    private int songId;
    private String songName;
    private String artist;
    private String album;
    private String genre;
    private String time;
    private String status;
    private String file;
    private String albumArtwork;

    public Song() {}

    public Song(int songId, String songName, String artist, String album, String genre, String time, String status, String file, String albumArtwork) {
        this.songId = songId;
        this.songName = songName;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
        this.time = time;
        this.status = status;
        this.file = file;
        this.albumArtwork = albumArtwork;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getAlbumArtwork() {
        return albumArtwork;
    }

    public void setAlbumArtwork(String albumArtwork) {
        this.albumArtwork = albumArtwork;
    }


    public int getSongId() {
        return songId;
    }

    public void setSongId(int songId) {
        this.songId = songId;
    }


}
