package com.example.methodologyproject;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "songDB.db";
    public static final String TABLE_NAME = "Song";
    public static final String COLUMN_SONGID = "SongID";
    public static final String COLUMN_SONGNAME = "SongName";
    public static final String COLUMN_ARTIST = "Artist";
    public static final String COLUMN_ALBUM = "Album";
    public static final String COLUMN_GENRE = "Genre";
    public static final String COLUMN_TIME = "Time";
    public static final String COLUMN_STATUS = "Status";
    public static final String COLUMN_FILE  ="File";
    public static final String COLUMN_AA = "AlbumArtwork";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + COLUMN_SONGID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_SONGNAME + " TEXT, " + COLUMN_ARTIST + " TEXT, " + COLUMN_ALBUM + " TEXT, " + COLUMN_GENRE + " TEXT, " + COLUMN_TIME + " TEXT, " + COLUMN_STATUS + " TEXT, " + COLUMN_FILE + " TEXT, " + COLUMN_AA + " TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {}

    public String loadGenres() {
        String result = "";
        String query = "Select " + COLUMN_GENRE + " FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String result1 = cursor.getString(0);
            result += (result1) + "; " +
                    System.getProperty("line.separator");
        }
        cursor.close();
        db.close();
        return result;
    }

    public String loadArtists() {
        String result = "";
        String query = "Select " + COLUMN_ARTIST + " FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String result1 = cursor.getString(0);
            result += (result1) + "; " +
                    System.getProperty("line.separator");
        }
        cursor.close();
        db.close();
        return result;
    }

    public String loadAlbums() {
        String result = "";
        String query = "Select " + COLUMN_ALBUM + " FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String result1 = cursor.getString(0);
            result += (result1) + "; " +
                    System.getProperty("line.separator");
        }
        cursor.close();
        db.close();
        return result;
    }

    public void addSongs(Song song) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SONGID, song.getSongId());
        values.put(COLUMN_SONGNAME, song.getSongName());
        values.put(COLUMN_ARTIST, song.getArtist());
        values.put(COLUMN_ALBUM, song.getAlbum());
        values.put(COLUMN_GENRE, song.getGenre());
        values.put(COLUMN_TIME, song.getTime());
        values.put(COLUMN_STATUS, song.getStatus());
        values.put(COLUMN_FILE, song.getFile());
        values.put(COLUMN_AA, song.getAlbumArtwork());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public String genreQueryDB(ArrayList<String> likedGenreList) {
        String result = "";

        String list = "";

        for(int i=0; i<likedGenreList.size(); i++) {

            if(i == 0) { //first element in list
                list += " WHERE " + COLUMN_GENRE + " = '" + likedGenreList.get(i) + "'";
            } else { //middle elements in list if length>1
                list += " AND " + COLUMN_GENRE + " = '" + likedGenreList.get(i) + "'";
            }
        }

        String query = "SELECT " + COLUMN_SONGNAME + ", " + COLUMN_ARTIST + " FROM " + TABLE_NAME + list;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String result1 = cursor.getString(0);
            result += (result1) + "; " + System.getProperty("line.separator");
        }
        cursor.close();
        db.close();
        return result;
    }

    public String artistQueryDB(ArrayList<String> likedArtistList) {
        String result = "";

        String list = "";

        for(int i=0; i<likedArtistList.size(); i++) {

            if(i == 0) { //first element in list
                list += " WHERE " + COLUMN_ARTIST + " = '" + likedArtistList.get(i) + "'";
            } else { //middle elements in list if length>1
                list += " AND " + COLUMN_ARTIST + " = '" + likedArtistList.get(i) + "'";
            }
        }

        String query = "SELECT " + COLUMN_SONGNAME + ", " + COLUMN_ARTIST + " FROM " + TABLE_NAME + list;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String result1 = cursor.getString(0);
            result += (result1) + "; " + System.getProperty("line.separator");
        }
        cursor.close();
        db.close();
        return result;
    }

    public String albumQueryDB(ArrayList likedAlbumList) {
        String result = "";

        String list = "";

        for(int i=0; i<likedAlbumList.size(); i++) {

            if(i == 0) { //first element in list
                list += " WHERE " + COLUMN_ALBUM + " = '" + likedAlbumList.get(i) + "'";
            } else { //middle elements in list if length>1
                list += " AND " + COLUMN_ALBUM + " = '" + likedAlbumList.get(i) + "'";
            }
        }

        String query = "SELECT " + COLUMN_SONGNAME + ", " + COLUMN_ARTIST + " FROM " + TABLE_NAME + list;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String result1 = cursor.getString(0);
            result += (result1) + "; " + System.getProperty("line.separator");
        }
        cursor.close();
        db.close();
        return result;
    }
}
