package com.example.methodologyproject;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Player.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Player#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Player extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int homeX = 1050;
    private  int homeY = 1050;
    private double margin = homeX/2;
    private int returnSpeed = 500;
    private Integer setting;
    private int LEFT = 0;
    private int RIGHT = 1;
    private int UP = 2;
    private int DOWN = 3;
    private int NEUTRAL = -1;
    private OnFragmentInteractionListener mListener;

    public Player() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Player.
     */
    // TODO: Rename and change types and number of parameters
    public static Player newInstance(String param1, String param2) {
        Player fragment = new Player();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_player, container, false);


        //get horizontal and vertical scroll views
        final HorizontalScrollView XScroller = view.findViewById(R.id.hori);
        final NestedScrollView YScroller = view.findViewById(R.id.vert);
        final Button Center = view.findViewById(R.id.center);

        //set initial positions of scroll wheels
        XScroller.post(new Runnable() {
            public void run() {
                XScroller.setScrollX(homeX);
            }
        });
        YScroller.post(new Runnable() {
            public void run() {
                YScroller.setScrollY(homeY);
            }
        });


        //set action events
        XScroller.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(XScroller.getScrollX()< margin){
                    //Toast toast = Toast.makeText(getContext(), "RIGHT SWIPE", Toast.LENGTH_LONG);
                    //toast.show();
                    setting = RIGHT;
                } else if(XScroller.getScrollX()> homeX * 2-margin){
                    //Toast toast = Toast.makeText(getContext(), "LEFT SWIPE", Toast.LENGTH_LONG);
                    //toast.show();
                    setting = LEFT;
                } else {
                    //Toast toast = Toast.makeText(getContext(), "RETURN HOME", Toast.LENGTH_LONG);
                    //toast.show();
                    setting = NEUTRAL;
                }
            }
        });
        YScroller.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(YScroller.getScrollY()< margin){
                    //Toast toast = Toast.makeText(getContext(), "DOWN SWIPE", Toast.LENGTH_LONG);
                    //toast.show();
                    setting = DOWN;
                } else if(YScroller.getScrollY()> homeY * 2-margin){
                    //Toast toast = Toast.makeText(getContext(), "UP SWIPE", Toast.LENGTH_LONG);
                    //toast.show();
                    setting = UP;
                } else {
                    //Toast toast = Toast.makeText(getContext(), "RETURN HOME", Toast.LENGTH_LONG);
                    //toast.show();
                    setting = NEUTRAL;
                }
            }
        });

        //return to center
        XScroller.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    Toast toast = Toast.makeText(getContext(), (setting).toString(), Toast.LENGTH_SHORT);
                    toast.show();
                    ObjectAnimator.ofInt(YScroller, "scrollY", homeY ).setDuration(returnSpeed).start();
                    ObjectAnimator.ofInt(XScroller, "scrollX", homeX ).setDuration(returnSpeed).start();
                    if(setting != NEUTRAL){
                        //TODO
                        //
                        //put new artist on the center button
                    }
                }
                return false;
            }
        });
        YScroller.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    Toast toast = Toast.makeText(getContext(), (setting).toString(), Toast.LENGTH_LONG);
                    toast.show();
                    ObjectAnimator.ofInt(YScroller, "scrollY", homeY ).setDuration(returnSpeed).start();
                    ObjectAnimator.ofInt(XScroller, "scrollX", homeX ).setDuration(returnSpeed).start();
                    if(setting != NEUTRAL){
                        //TODO
                        //call method passing in song and setting to algorithm
                        //put new artist on the center button
                    }
                }
                return false;
            }
        });





        return  view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
