package com.example.methodologyproject;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static View view;
    static ImageButton play;
    static ImageButton back;
    static ImageButton forward;
    static LinearLayout playerButtons;
    static LinearLayout player;
    static SeekBar seekBar;
    static TextView songTitle;
    static Boolean inPlayer = false;
    static int songIndex;
    static ArrayList<String> songName;
    static ArrayList<String> artist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //set view from activity_main.xml
        super.onCreate(savedInstanceState);
        //Create a new local database.


        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.activity_main, null );
        setContentView(view);

        //find buttons for player
        play = view.findViewById(R.id.imageButton);
        back = view.findViewById(R.id.imageButton2);
        forward = view.findViewById(R.id.imageButton3);
        playerButtons = view.findViewById(R.id.innerLayout);
        player = view.findViewById(R.id.outerLayout);
        seekBar = view.findViewById(R.id.seekBar);
        songTitle = view.findViewById(R.id.title);

        songIndex = 0;

        //set actions of buttons and seekbar
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //play music

            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //next song
                songIndex++;
                displaySongInfo(songIndex, songName, artist);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //previous song
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        //set homepage in 3s
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (getSupportFragmentManager().findFragmentById(android.R.id.content) == null) {
                    getSupportFragmentManager().beginTransaction()
                            .add(android.R.id.content, new Home())
                            .commit();
                }
            }
        }, 3000);

        //final String sender = this.getIntent().getExtras().getString("Home");
        //if(sender != null) {
            //receiveData();
        //}

        Bundle bundle= getIntent().getExtras();
        if(bundle != null) {
            receiveData();
        }
    }

    public static void RaisePlayerButtons(){

        ValueAnimator animator = ValueAnimator.ofInt(seekBar.getPaddingBottom(), 100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator){
                seekBar.setPadding(0, 0, 0, (Integer) valueAnimator.getAnimatedValue());
                songTitle.setPadding(0,0,0, (Integer) valueAnimator.getAnimatedValue());
            }
        });
        animator.setDuration(600);
        animator.start();

        ObjectAnimator.ofFloat(player, "TranslationY",   -200).setDuration(600).start();

        inPlayer = true;


    }
    public static void LowerPlayerButtons(){
        ValueAnimator animator = ValueAnimator.ofInt(seekBar.getPaddingBottom(), 0);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator){
                seekBar.setPadding(0, 0, 0, (Integer) valueAnimator.getAnimatedValue());
                songTitle.setPadding(0,0,0, (Integer) valueAnimator.getAnimatedValue());
            }
        });
        animator.setDuration(600);
        animator.start();
        ObjectAnimator.ofFloat(player, "TranslationY",   0).setDuration(600).start();
    }

    @Override
    public void onBackPressed()
    {
        if(inPlayer){
            LowerPlayerButtons();
            inPlayer = false;
        }
        super.onBackPressed();
    }

    private void receiveData() {
        //RECEIVE DATA VIA INTENT
        Intent i = getIntent();
        songName = i.getStringArrayListExtra("SONG_NAME");
        artist = i.getStringArrayListExtra("ARTIST");

        //SET DATA TO TEXTVIEW
        displaySongInfo(songIndex, songName, artist);
    }

    private void displaySongInfo(int songIndex, ArrayList songName, ArrayList artist) {
        songTitle.setText(artist.get(songIndex) + " - " + songName.get(songIndex));
    }
}
