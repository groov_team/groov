package com.example.methodologyproject;


import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.Toast;
import android.content.Context;

import java.lang.reflect.Array;
import java.util.*;


/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {
    //initial positions of scroll wheels
    private int homeX = 1050;
    private  int homeY = 0;
    private int currentY = 0;
    private double margin = homeX/2;
    private int returnSpeed = 500;
    private Integer setting;
    private final int LEFT = 0;
    private final int RIGHT = 1;
    private final int UP = 2;
    private final int DOWN = 3;
    private final int NEUTRAL = -1;
    public DBHelper database;



    public Home() {
        // Required empty public constructor
    }

    public DBHelper loadDatabase(){
        DBHelper localDatabase = new DBHelper(super.getContext(), null, null, 1);
        //localDatabase.getWritableDatabase();
        Song firstSong = new Song((int)(Math.random()*100) ,"Shallon Is The Bees-Knees", "The Bops", "I just want to pop", "K-POP", "2:30", "L", "file.mp3", "album art 1");
        Song secondSong = new Song((int)(Math.random()*100) ,"Beautiful Girls", "The Killers", "I just want to rap", "RAP", "2:55", "L", "file1.mp3", "album art 1");
        Song thirdSong = new Song((int)(Math.random()*100) ,"Get groovy, baby", "The Rockers", "I just want to rock", "ROCK", "3:30", "L", "file2.mp3", "album art 1");
        localDatabase.addSongs(firstSong);
        localDatabase.addSongs(secondSong);
        localDatabase.addSongs(thirdSong);

        return localDatabase;
    }

    //Temporary Local data-base while the real one is still being constructed.
    public ArrayList<String> getGenres(){
        ArrayList<String> lst = new ArrayList();
        lst.add("K-POP");
        lst.add("RAP");
        lst.add("ROCK");
        return lst;
    }

    //Temporary Local data-base while the real one is still being constructed.
    public ArrayList<String> getAlbums(){
        ArrayList<String> lst = new ArrayList();
        lst.add("I just want to pop");
        lst.add("I just want to rap");
        lst.add("I just want to rock");
        return lst;
    }

    //Temporary Local data-base while the real one is still being constructed.
    public ArrayList<String> getArtist(){
        ArrayList<String> lst = new ArrayList();
        lst.add("The Bops");
        lst.add("The Killers");
        lst.add("The Rockers");
        return lst;
    }

    //Temporary Local data-base while the real one is still being constructed.
    public ArrayList<String> addASong(String newSong){
        ArrayList<String> lst = new ArrayList();
        lst.add("Shallon Is The Bees-Knees");
        lst.add("Beautiful Girls");
        lst.add("Get groovy, baby");
        lst.add(newSong);
        return lst;
    }




    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        //get horizontal and vertical scroll views
        final HorizontalScrollView XScroller = view.findViewById(R.id.hori);
        final NestedScrollView YScroller = view.findViewById(R.id.vert);
        final Button groov = view.findViewById(R.id.Groov);
         Button resume = view.findViewById(R.id.Resume);

        //set initial positions of scroll wheels
        XScroller.post(new Runnable() {
            public void run() {
                XScroller.setScrollX(homeX);
            }
        });
        YScroller.post(new Runnable() {
            public void run() {
                YScroller.setScrollY(homeY);
            }
        });


        //TODO
        //Set inital values of ScrollView Items
        //Query random artist from DB and set to Text of Each Item
        database = loadDatabase();

        final List<String> myGenreList = new ArrayList<String>(Arrays.asList(database.loadGenres().split("; ")));
        final List<String> myAlbumList = new ArrayList<String>(Arrays.asList(database.loadAlbums().split("; ")));
        final List<String> myArtistList = new ArrayList<String>(Arrays.asList(database.loadArtists().split("; ")));


        final Button genreButton = view.findViewById(R.id.button5);
        genreButton.setText(myGenreList.get(0));

        final Button artistButton = view.findViewById(R.id.center);
        artistButton.setText(myArtistList.get(0));

        final Button albumButton = view.findViewById(R.id.button7);
        albumButton.setText(myAlbumList.get(0));

        final ArrayList<String> likedGenreList = new ArrayList<>();
        final ArrayList<String> dislikedGenreList = new ArrayList<>();
        final ArrayList<String> likedArtistList = new ArrayList<>();
        final ArrayList<String> dislikedArtistList = new ArrayList<>();
        final ArrayList<String> likedAlbumList = new ArrayList<>();
        final ArrayList<String> dislikedAlbumList = new ArrayList<>();

        //set action events
        XScroller.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(XScroller.getScrollX()< margin){
                    setting = RIGHT;
                } else if(XScroller.getScrollX()> homeX * 2-margin){
                    setting = LEFT;
                } else {
                    setting = NEUTRAL;
                }
            }
        });
        YScroller.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(YScroller.getScrollY()< currentY * 700 + 50){
                    currentY--;
                    setting = DOWN;
                } else if(YScroller.getScrollY()> (currentY + 1)*700 - 50){
                    currentY++;
                    setting = UP;
                } else {
                    setting = NEUTRAL;
                }
            }
        });

        //return to center
        XScroller.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int genreIndex = 0;
                int artistIndex = 0;
                int albumIndex = 0;

                if(event.getAction() == MotionEvent.ACTION_UP){
                    Toast toast = Toast.makeText(getContext(), (setting).toString(), Toast.LENGTH_SHORT);
                    toast.show();
                    ObjectAnimator.ofInt(YScroller, "scrollY", currentY * 1050 ).setDuration(returnSpeed).start();
                    ObjectAnimator.ofInt(XScroller, "scrollX", homeX ).setDuration(returnSpeed).start();
                    if(setting != NEUTRAL){
                        //TODO
                        //call method passing in value and setting to algorithm
                        //put new text/image on xml item
                        //pass in SETTING to say like/dislike
                        //pass in CURRENTY to say if genre/album/artist
                        //pass in ITEM.TEXT to say which item you are setting
                        //
                        //pseudocode
                        //--------------------------------
                        //Input(setting, currentY,item.text)
                        //item.text = GetRandom(currentY)

                        //Adding String to Dislike/Liked ArrayList to later be sent to query
                        switch(setting){
                            case LEFT:
                                switch (currentY){
                                    case 0:
                                        dislikedGenreList.add(genreButton.getText().toString());
                                        break;
                                    case 1:
                                        dislikedArtistList.add(artistButton.getText().toString());
                                        break;
                                    case 2:
                                        dislikedAlbumList.add(albumButton.getText().toString());
                                        break;
                                }
                                break;
                            case RIGHT:
                                switch (currentY){
                                    case 0:
                                        likedGenreList.add(genreButton.getText().toString());
                                        break;
                                    case 1:
                                        likedArtistList.add(artistButton.getText().toString());
                                        break;
                                    case 2:
                                        likedAlbumList.add(albumButton.getText().toString());
                                        break;
                                }
                                break;
                        }


                        //
                        switch (currentY){
                            case 0:
                                //genre button text change
                                if(genreIndex <= myGenreList.size()) {
                                    genreIndex++;
                                    genreButton.setText(myGenreList.get(genreIndex));
                                } else {
                                    //remove genreButton
                                }


                                break;
                            case 1:
                                //artist  button text change
                                if(artistIndex <= myArtistList.size()) {
                                    artistIndex++;
                                    artistButton.setText(myArtistList.get(artistIndex));
                                } else {
                                    //remove artistButton
                                }

                                break;
                            case 2:
                                //album  button text change
                                if(albumIndex <= myAlbumList.size()) {
                                    albumIndex++;
                                    albumButton.setText(myAlbumList.get(albumIndex));
                                } else {
                                    //remove artistButton
                                }

                                break;
                        }

                    }
                }
                return false;
            }
        });
        YScroller.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    Toast toast = Toast.makeText(getContext(), (setting).toString(), Toast.LENGTH_LONG);
                    toast.show();
                    //snap to album/artist/genre
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            ObjectAnimator.ofInt(YScroller, "scrollY", currentY * 1050 ).setDuration(returnSpeed/2).start();
                            ObjectAnimator.ofInt(XScroller, "scrollX", homeX ).setDuration(returnSpeed/2).start();
                        }
                    }, 300);
                }
                return false;
            }
        });
        //create new player and process new groov
        groov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call query methods
                String returnedSongs = database.genreQueryDB(likedGenreList);
                String returnedSongs2 = database.artistQueryDB(likedArtistList);
                String returnedSongs3 = database.albumQueryDB(likedAlbumList);

                //this variable holds all queried songs based on swipes
                String totalReturnedSongs = returnedSongs + returnedSongs2 + returnedSongs3;
                final List<String> returnedSongsList = new ArrayList<String>(Arrays.asList(totalReturnedSongs.split("; ")));
                ArrayList<String> returnedSongsNameList = new ArrayList<String>();
                ArrayList<String> returnedSongsArtistList = new ArrayList<String>();
                for(int i = 0; i<returnedSongsList.size(); i+=2) {
                    returnedSongsNameList.add(returnedSongsList.get(i));
                    if(i == returnedSongsList.size()-1) {
                        break;
                    } else {
                        returnedSongsArtistList.add(returnedSongsList.get(i+1));
                    }

                }

                //pass returnedSongsNameList and returnedSongsArtistList to display songs
                sendData(returnedSongsNameList, returnedSongsArtistList);

                //go to next screen
                MainActivity.RaisePlayerButtons();
                Fragment fragment = new Player();
                FragmentManager fragmentManager =
                        getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =
                        fragmentManager.beginTransaction();
                fragmentTransaction.replace(
                        android.R.id.content, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        //resume player/groov
        //TODO reload current player from saved
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.RaisePlayerButtons();
                Fragment fragment = new Player();
                FragmentManager fragmentManager =
                        getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =
                        fragmentManager.beginTransaction();
                fragmentTransaction.replace(
                        android.R.id.content, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });






        return  view;
    }

    private void sendData(ArrayList returnedSongsNameList, ArrayList returnedSongsArtistList)
    {
        //INTENT OBJ
        Intent i = new Intent(getActivity().getBaseContext(), MainActivity.class);

        //PACK DATA
        i.putExtra("SENDER_KEY", "Home");
        i.putStringArrayListExtra("SONG_NAME", returnedSongsNameList);
        i.putStringArrayListExtra("ARTIST", returnedSongsArtistList);

        //START ACTIVITY
        getActivity().startActivity(i);
    }

}
