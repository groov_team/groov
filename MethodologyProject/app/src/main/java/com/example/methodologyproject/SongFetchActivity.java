package com.example.methodologyproject;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class SongFetchActivity extends AsyncTask {

    public SongFetchActivity() {

    }

    /*@Override
   protected String doInBackground(String... arg0) {
        return null;
    }

    protected void onPreExecute(){
    }*/

    @Override
    protected Object doInBackground(Object[] objects) {
        try{
            String genre = (String)objects[0];
            String artist = (String)objects[1];
            String album = (String)objects[2];
            String link = "http://127.0.0.1/phpmyadmin/fetch_songs.php?genre="+genre+"& artist="+artist+"& album="+album;

            URL url = new URL(link);
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI(link));
            HttpResponse response = client.execute(request);
            BufferedReader in = new BufferedReader(new
                    InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line="";

            while ((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }

            in.close();
            return sb.toString();
        } catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }
    }

    //@Override
    protected void onPostExecute(String result){
        //this.statusField.setText("Login Successful");
        //this.roleField.setText(result);
    }

}
